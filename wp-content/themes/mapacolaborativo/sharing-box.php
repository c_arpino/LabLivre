<div id="social-share" style="margin-top:30px">
<h3>Compartilhe!</h3>
<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css' type='text/css' media='all' />
<a href="whatsapp://send?text=<?php the_title(); ?> - <?php the_permalink(); ?>" data-action="share/whatsapp/share"><i class="fa fa-2x fa-fw fa-lg fa-whatsapp text-danger"></i></a>
<a href="https://twitter.com/home/?status=<?php the_title(); ?> - <?php the_permalink(); ?>" title="Tweet!"><i class="fa fa-2x fa-fw fa-lg fa-twitter text-danger"></i></a>
<a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" title="Compartilhe no Facebook!"><i class="fa fa-2x fa-fw fa-lg fa-facebook text-danger"></i></a>
<a href="https://t.me/share/url?url=<?php the_permalink(); ?>&text=<?php the_title(); ?>" target="_blank" class="telegram"><i class="fa fa-2x fa-fw fa-lg fa-telegram"></i></a>
</div>
