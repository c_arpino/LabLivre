class Filtros {

    tiposDeAcoes() {
        return {
            coletivo: [
                'Alimentação',
                'Arrecadação', 
                'Assistência Social', 
                'Combate a Violência Doméstica', 
                'Cuidados', 
                'Cultura',
                'Doações',
                'Educação',
                'Entretenimento',
                'Informação',
                'Maker/Hacker',
                'Moradia',
                'Redes de Solidariedade',
                'Saúde',
                'Trabalho e Renda',
                'Outras'
            ],
            universidade: [
                'Realizando Pesquisas Sobre', 
                'Produzindo Equipamentos', 
                'Produzindo Medicamentos',
                'Confeccionando Produtos',
                'Prestando Serviços', 
                'Outras Ações'
            ]
        }
    }

    limparFiltros() {
        document.querySelector('#categoria').value = 'selecione';
        document.getElementById('tipo-acao').options.length = 1;
        document.getElementById('tipo-acao').hidden = true;
        document.getElementById('limpar-filtro').hidden = true;
    }

    eventoTrocaCategoria() {
        let categoriaSelecionada = document.querySelector('#categoria').value;

        if (categoriaSelecionada == 'selecione') {
            this.limparFiltros();
            return null;
        }

        document.getElementById('tipo-acao').hidden = false;
        document.getElementById('limpar-filtro').hidden = false;
        document.getElementById('tipo-acao').options.length = 1;
        this.constroiFiltroTipodeAcao(categoriaSelecionada);

        return categoriaSelecionada;
    }

    constroiFiltroTipodeAcao(categoria) {
        this.tiposDeAcoes()[categoria].forEach(acao => {
            let option = document.createElement('option');
            option.value = acao;
            option.textContent = acao;
            document.querySelector('#tipo-acao').appendChild(option);
        })
    }

}