<?php

/**

 * @author Divi Space

 * @copyright 2017

 */

if (!defined('ABSPATH')) die();

function ds_ct_enqueue_parent() { wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' ); }

function ds_ct_loadjs() {
	wp_enqueue_script( 'ds-theme-script', get_stylesheet_directory_uri() . '/ds-script.js', array( 'jquery' )  );
	wp_enqueue_script( 'mapa-helper', get_stylesheet_directory_uri() . '/mapa-helper.js', array( 'jquery' )  );
	wp_enqueue_script( 'filtros-helper', get_stylesheet_directory_uri() . '/filtros-helper.js', array( 'jquery' )  );
	//wp_enqueue_script('leaflet', get_stylesheet_directory_uri() . '/assets/js/leaflet-src.js', array() );
	//wp_enqueue_script('leaflet.markercluster', get_stylesheet_directory_uri() . '/assets/js/leaflet.markercluster-src.js', array() );

}


add_action( 'wp_enqueue_scripts', 'ds_ct_enqueue_parent' );

add_action( 'wp_enqueue_scripts', 'ds_ct_loadjs' );

