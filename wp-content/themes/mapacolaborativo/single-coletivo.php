<?php

get_header();

$show_default_title = get_post_meta( get_the_ID(), '_et_pb_show_title', true );

$is_page_builder_used = et_pb_is_pagebuilder_used( get_the_ID() );

?>

<div id="main-content">

	<?php
		if ( et_builder_is_product_tour_enabled() ):
			// load fullwidth page in Product Tour mode
			while ( have_posts() ): the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class( 'et_pb_post' ); ?>>
					<div class="entry-content">
					<?php
						the_content();
					?>

					</div> <!-- .entry-content -->

				</article> <!-- .et_pb_post -->

		<?php endwhile;
		else:
	?>
	<div class="container">
		<div id="content-area" class="clearfix">
			<div id="left-area">
			<?php while ( have_posts() ) : the_post(); ?>
				<?php
				/**
				 * Fires before the title and post meta on single posts.
				 *
				 * @since 3.18.8
				 */
				do_action( 'et_before_post' );
				?>
				<article id="post-<?php the_ID(); ?>" <?php post_class( 'et_pb_post' ); ?>>
					<?php if ( ( 'off' !== $show_default_title && $is_page_builder_used ) || ! $is_page_builder_used ) { ?>
						<div class="et_post_meta_wrapper">
							<h1 class="entry-title"><?php the_title(); ?></h1>

<h3 style="margin-top:15px">Informações da Ação</h3>
<?php if( get_field('publico_da_acao') ): ?>
	<p><strong>PÚBLICO DA AÇÃO:</strong> <?php the_field('publico_da_acao'); ?></p>
<?php endif; ?>

<?php if( get_field('tipo_de_organizacao') ): ?>
	<p><strong>TIPO DE ORGANIZAÇÃO:</strong> <?php the_field('tipo_de_organizacao'); ?></p>
<?php endif; ?>

<?php if( get_field('organizacao_outras') ): ?>
	<p><strong>TIPO DE ORGANIZAÇÃO:</strong> <?php the_field('organizacao_outras'); ?></p>
<?php endif; ?>

<p>
<?php if( get_field('area_de_atuacao') ): ?>
	<strong>ÁREA DE ATUAÇÃO:</strong> <?php the_field('area_de_atuacao'); ?>
<?php endif; ?>

<?php if( get_field('atuacao_outras') ): ?>
	<?php the_field('atuacao_outras'); ?>
<?php endif; ?></p>

<?php if( get_field('como_ajudar_1') ): ?>
	<p><strong>Como ajudar</strong>: <?php the_field('como_ajudar_1'); ?>
<?php endif; ?>

<?php if( get_field('como_ajudar_2') ): ?>
	, <?php the_field('como_ajudar_2'); ?>
<?php endif; ?>

<?php if( get_field('como_ajudar_3') ): ?>
	, <?php the_field('como_ajudar_3'); ?>
<?php endif; ?></p>

<?php if( get_field('acao_desenvolvida') ): ?>
	<p><strong>Ação desenvolvida:</strong> <?php the_field('acao_desenvolvida'); ?></p>
<?php endif; ?>

<h3>Contatos</h3>

<?php if( get_field('nome_do_contato') ): ?>
	<p><strong>NOME DO CONTATO:</strong> <?php the_field('nome_do_contato'); ?></p>
<?php endif; ?>

<?php if (get_field('site')) : ?>
    <p><strong>SITE:</strong> <a href="<?php the_field('site'); ?>" ><?php the_field('site'); ?></a></p>
<?php endif; ?>

<?php if( get_field('facebook') ): ?>
	<p><strong>FACEBOOK:</strong> <a href="<?php the_field('facebook'); ?>" ><?php the_field('facebook'); ?></a></p>
<?php endif; ?>

<?php if( get_field('youtube') ): ?>
	<p><strong>YOUTUBE: <a href="<?php the_field('youtube'); ?>" ><?php the_field('youtube'); ?></a></p>
<?php endif; ?>

<?php if( get_field('twitter') ): ?>
	<p><strong>TWITTER:</strong> <a href="<?php the_field('twitter'); ?>" ><?php the_field('twitter'); ?></a></p>
<?php endif; ?>

<?php if( get_field('instagram') ): ?>
	<p><strong>INSTAGRAM:</strong> <a href="<?php the_field('instagram'); ?>" ><?php the_field('instagram'); ?></a></p>
<?php endif; ?>

<?php if( get_field('e-mail') ): ?>
	<p><strong>E-MAIL:</strong> <?php the_field('e-mail'); ?></p>
<?php endif; ?>

<?php if( get_field('celular') ): ?>
	<p><strong>CELULAR:</strong> <?php the_field('celular'); ?></p>
<?php endif; ?>

<?php if( get_field('whatsapp') ): ?>
	<p><strong>WHATSAPP:</strong> <?php the_field('whatsapp'); ?></p>
<?php endif; ?>

<h2>Endereço</h2>
<?php if( get_field('rua') ): ?>
	<?php the_field('rua'); ?>
<?php endif; ?>

<?php if( get_field('numero') ): ?>
	, <?php the_field('numero'); ?>
<?php endif; ?><br />

<?php if( get_field('complemento') ): ?>
	, <?php the_field('complemento'); ?>
<?php endif; ?><br />

<?php if( get_field('bairro') ): ?>
	<?php the_field('bairro'); ?> - 
<?php endif; ?>

<?php if( get_field('cidade') ): ?>
	<?php the_field('cidade'); ?>
<?php endif; ?>

<?php if( get_field('estado') ): ?>
	/ <?php the_field('estado'); ?><br />
<?php endif; ?>

<?php if( get_field('cep') ): ?>
	<p>CEP: <?php the_field('cep'); ?></p>
<?php endif; ?>


<?php get_template_part('sharing', 'box'); ?>



						<?php
							if ( ! post_password_required() ) :

								//et_divi_post_meta();

								$thumb = '';

								$width = (int) apply_filters( 'et_pb_index_blog_image_width', 1080 );

								$height = (int) apply_filters( 'et_pb_index_blog_image_height', 675 );
								$classtext = 'et_featured_image';
								$titletext = get_the_title();
								$alttext = get_post_meta( get_post_thumbnail_id(), '_wp_attachment_image_alt', true );
								$thumbnail = get_thumbnail( $width, $height, $classtext, $alttext, $titletext, false, 'Blogimage' );
								$thumb = $thumbnail["thumb"];

								$post_format = et_pb_post_format();

								if ( 'video' === $post_format && false !== ( $first_video = et_get_first_video() ) ) {
									printf(
										'<div class="et_main_video_container">
											%1$s
										</div>',
										et_core_esc_previously( $first_video )
									);
								} else if ( ! in_array( $post_format, array( 'gallery', 'link', 'quote' ) ) && 'on' === et_get_option( 'divi_thumbnails', 'on' ) && '' !== $thumb ) {
									print_thumbnail( $thumb, $thumbnail["use_timthumb"], $alttext, $width, $height );
								} else if ( 'gallery' === $post_format ) {
									et_pb_gallery_images();
								}
							?>

							<?php
								$text_color_class = et_divi_get_post_text_color();

								$inline_style = et_divi_get_post_bg_inline_style();

								switch ( $post_format ) {
									case 'audio' :
										$audio_player = et_pb_get_audio_player();

										if ( $audio_player ) {
											printf(
												'<div class="et_audio_content%1$s"%2$s>
													%3$s
												</div>',
												esc_attr( $text_color_class ),
												et_core_esc_previously( $inline_style ),
												et_core_esc_previously( $audio_player )
											);
										}

										break;
									case 'quote' :
										printf(
											'<div class="et_quote_content%2$s"%3$s>
												%1$s
											</div> <!-- .et_quote_content -->',
											et_core_esc_previously( et_get_blockquote_in_content() ),
											esc_attr( $text_color_class ),
											et_core_esc_previously( $inline_style )
										);

										break;
									case 'link' :
										printf(
											'<div class="et_link_content%3$s"%4$s>
												<a href="%1$s" class="et_link_main_url">%2$s</a>
											</div> <!-- .et_link_content -->',
											esc_url( et_get_link_url() ),
											esc_html( et_get_link_url() ),
											esc_attr( $text_color_class ),
											et_core_esc_previously( $inline_style )
										);

										break;
								}

							endif;
						?>
					</div> <!-- .et_post_meta_wrapper -->
				<?php  } ?>

					<div class="entry-content">



					<?php
						do_action( 'et_before_content' );

						//the_content();

						wp_link_pages( array( 'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'Divi' ), 'after' => '</div>' ) );
					?>
					</div> <!-- .entry-content -->
					<div class="et_post_meta_wrapper">
					<?php
					if ( et_get_option('divi_468_enable') === 'on' ){
						echo '<div class="et-single-post-ad">';
						if ( et_get_option('divi_468_adsense') !== '' ) echo et_core_intentionally_unescaped( et_core_fix_unclosed_html_tags( et_get_option('divi_468_adsense') ), 'html' );
						else { ?>
							<a href="<?php echo esc_url(et_get_option('divi_468_url')); ?>"><img src="<?php echo esc_attr(et_get_option('divi_468_image')); ?>" alt="468" class="foursixeight" /></a>
				<?php 	}
						echo '</div> <!-- .et-single-post-ad -->';
					}

					/**
					 * Fires after the post content on single posts.
					 *
					 * @since 3.18.8
					 */
					do_action( 'et_after_post' );

						if ( ( comments_open() || get_comments_number() ) && 'on' === et_get_option( 'divi_show_postcomments', 'on' ) ) {
							comments_template( '', true );
						}
					?>
					</div> <!-- .et_post_meta_wrapper -->
				</article> <!-- .et_pb_post -->

			<?php endwhile; ?>
			</div> <!-- #left-area -->

			<?php get_sidebar(); ?>
		</div> <!-- #content-area -->
	</div> <!-- .container -->
	<?php endif; ?>
</div> <!-- #main-content -->

<?php

get_footer();
