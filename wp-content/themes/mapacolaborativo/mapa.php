<?php /* Template Name: Mapa */ ?>

<?php
$is_page_builder_used = et_pb_is_pagebuilder_used(get_the_ID());
?>

<!DOCTYPE html>
<html>

<head>
  <meta charset=utf-8 />
  <title>Mapa Colaborativo</title>
  <meta name='viewport' content='initial-scale=1,maximum-scale=1,user-scalable=no' />

  <script src='https://api.mapbox.com/mapbox-gl-js/v1.11.0/mapbox-gl.js'></script>
  <link href='https://api.mapbox.com/mapbox-gl-js/v1.11.0/mapbox-gl.css' rel='stylesheet' />

  <!-- <script src='https://api.mapbox.com/mapbox.js/v3.3.0/mapbox.js'></script>
  <link href='https://api.mapbox.com/mapbox.js/v3.3.0/mapbox.css' rel='stylesheet' />
  <script src='https://api.mapbox.com/mapbox.js/plugins/leaflet-markercluster/v1.0.0/leaflet.markercluster.js'></script>
  <link href='https://api.mapbox.com/mapbox.js/plugins/leaflet-markercluster/v1.0.0/MarkerCluster.css' rel='stylesheet' />
  <link href='https://api.mapbox.com/mapbox.js/plugins/leaflet-markercluster/v1.0.0/MarkerCluster.Default.css' rel='stylesheet' /> -->

  <style>
    body {
      margin: 0;
      padding: 0;
    }

    #map {
      position: absolute;
      top: 0;
      bottom: 0;
      width: 100%;
      z-index: 1;
    }

    #filtro {
      position: absolute;
      top: 0;
      padding-left: 12px;
      padding-top: 0;
      width: 220px;
      height: auto;
      z-index: 2;
      list-style-type: none;
    }

    select {
      width: 100%;
      border-width: 3px;
      margin-bottom: 3px;
      border-color: rgb(0, 82, 119);
      border-radius: 5px;
      padding: 10px;
      font-size: 15px;
      color: rgb(61, 73, 83);
    }

    #limpar-filtro {
      margin-top: 7px;
      width: 100%;
      border: 2px solid white;
      border-radius: 5px;
      background-color: rgb(0, 82, 119);
      padding: 10px;
      font-size: 15px;
      color: white;
      box-sizing: border-box;
      text-align: center;
    }
  </style>
</head>


<body>


  <div id='map'>
    <ul id='filtro'>
      <li>
        <select name="categoria" id="categoria" onchange="eventoTrocaCategoria()">
          <option value="selecione">Selecione a Categoria</option>
          <option value="coletivo">Coletivos e Movimentos</option>
          <option value="universidade">Universidades</option>
        </select>
      </li>
      <li>
        <select name="tipo-acao" id="tipo-acao" onchange="eventoTrocaTipoDeAcao()">
          <option value="selecione">Selecione o Tipo de Ação</option>
        </select>
      </li>
      <li>
        <div id="limpar-filtro" onclick="limparFiltros()">Limpar Filtros</div>
      </li>
    </ul>
  </div>

  <?php
  $args = array(
    'post_type' => array('universidade', 'coletivo'),
    'post_status' => 'publish',
    'posts_per_page' => -1,

  );
  query_posts($args);
  ?>

  <script src="<?php echo get_stylesheet_directory_uri(); ?>/mapa-helper.js"></script>
  <script src="<?php echo get_stylesheet_directory_uri(); ?>/filtros-helper.js"></script>

  <script>
    document.getElementById('tipo-acao').hidden = true;
    document.getElementById('limpar-filtro').hidden = true;
    var filtro = new Filtros();

    var enderecosGeoJson = {
      "type": "FeatureCollection",
      "features": [
        <?php if (have_posts()) : ?>
          <?php while (have_posts()) : the_post(); ?>
            <?php if (get_field('latitude') && is_numeric(trim(get_field('latitude'))) && get_field('longitude') && is_numeric(trim(get_field('longitude')))) : ?> {
                'type': 'Feature',
                'geometry': {
                  'type': 'Point',
                  'coordinates': ["<?php the_field('longitude'); ?>", "<?php the_field('latitude'); ?>"]
                },
                'properties': {
                  'id': "<?php the_ID(); ?>",
                  'categoria': "<?php echo get_post_type(); ?>",
                  'latitude': "<?php the_field('latitude'); ?>",
                  'longitude': "<?php the_field('longitude'); ?>",
                  'rua': "<?php the_field('rua'); ?>",
                  'numero': "<?php the_field('numero'); ?>",
                  'cidade': "<?php the_field('cidade'); ?>",
                  'estado': "<?php the_field('estado'); ?>",
                  'area_atuacao_coletivo': "<?php the_field('area_de_atuacao'); ?>",
                  'area_atuacao_universidade': "<?php the_field('como'); ?>",
                  'texto_alerta': "<strong><?php the_title(); ?></strong><br /><?php the_field('cidade'); ?> - <?php the_field('estado'); ?><br /><br /><a href='<?php the_permalink(); ?>' target='_parent'>ver mais</a>"
                }
              },
            <?php endif; ?>
          <?php endwhile; ?>
        <?php endif; ?>
      ]
    };

    const imageURL = "<?php echo get_stylesheet_directory_uri(); ?>/assets/image/pin.png";
    let mapaHelper = new MapaHelper();
    let source = 'iniciativas';
    let map = mapaHelper.criarMapa();
    escutarEventosDoMapa(map, source, enderecosGeoJson, imageURL);
    mapaHelper.obterLocalizacaoDoUsuario()
      .then(posicao => {
        const latitude = posicao.coords.latitude;
        const longitude = posicao.coords.longitude;
        mapaHelper.irPara(map, longitude, latitude, 6);
      })
      .catch(error => console.log(error));


    function escutarEventosDoMapa(map, source, enderecosGeoJson, imageURL) {
      mapaHelper.quandoOMapaCarregar(map, enderecosGeoJson, source, imageURL);
      mapaHelper.onClickNoCLuster(map, source);
      mapaHelper.onClickEmUmPontoNaoCLusterizado(map);
      // disable map zoom when using scroll
      map.scrollZoom.disable();
    }

    function eventoTrocaCategoria() {
      let categoria = filtro.eventoTrocaCategoria();
      mapaHelper.filtrarCategoria(map, categoria, source);
      popup_pin.remove();
    }

    function limparFiltros() {
      filtro.limparFiltros();
      mapaHelper.filtrarCategoria(map, null, source);
      popup_pin.remove();
    }

    function eventoTrocaTipoDeAcao() {
      let categoria = document.querySelector('#categoria').value;
      let acao = document.querySelector('#tipo-acao').value;
      mapaHelper.filtrarPorAcao(map, categoria, acao, source);
      popup_pin.remove();
    }
  </script>
</body>

</html>
