<?php /* Template Name: Todo Mundo */ ?>
<?php
//$is_page_builder_used = et_pb_is_pagebuilder_used( get_the_ID() );
?>
<?php
$args=array(
  'post_type' => array('universidade', 'coletivo'),
  'post_status' => 'publish',
  'posts_per_page' => -1,
);
query_posts($args);
?>
<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>
<?php if( get_field('latitude') ): ?>
L.marker([<?php the_field('latitude'); ?>,<?php the_field('longitude'); ?>]).addTo(mymap) .bindPopup("<b><?php the_title(); ?> </b><br><br>Resp.: <?php the_field('nome_do_contato'); ?><br><?php the_field('rua'); ?>, <?php the_field('numero'); ?><br><?php the_field('bairro'); ?><?php the_field('complemento'); ?><br><?php the_field('cidade'); ?> - <?php the_field('estado'); ?><br><br><?php if( get_field('celular') ): ?>Tel.: <?php the_field('celular'); ?><?php endif; ?>").openPopup();
<?php endif; ?>
<?php endwhile; ?>
<?php endif; ?>
