setup:
	docker-compose down --remove-orphans
	docker-compose up -d
	echo 'Waiting for DB initialization ... '
	sleep 10
	make restore

backup:
	docker exec db /usr/bin/mysqldump --databases -u wordpress --password=wordpress wordpress > init.sql

restore:
	cat init.sql | docker exec -i db /usr/bin/mysql -u wordpress --password=wordpress wordpress

status:
	docker ps

down:
	docker-compose down --remove-orphans
