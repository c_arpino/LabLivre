# Mapa Colaborativo

O Mapa Colaborativo é uma plataforma das práticas colaborativas de combate ao covid-19 e das redes de solidariedade.
Projeto do Laboratório de Tecnologias Livres da UFABC.

### Produção Coletiva de Conhecimento
Esta plataforma busca reunir as diversas práticas colaborativas que estão sendo realizadas em todas as regiões do Brasil para amenizar danos gerados pela pandemia de Covid-19. Disponibilizamos este espaço com o intuito de ajudarmos na promoção das ações e na captação de recursos, apoios e parcerias, bem como da criação de um registro histórico colaborativo de mobilizações da sociedade civil e das universidades frente à pandemia. A plataforma também permite que as pessoas encontrem os movimentos e os coletivos para ajudá-los na continuação de cada trabalho.

## Instalação

O ambiente de desenvolvimento utiliza o [Docker](https://www.docker.com/), para rodar o projeto, comece instalando a ferramenta por [aqui](https://www.docker.com/products/docker-desktop).

Agora com o docker instalado e funcionando, faça o clone do projeto pelo terminal:
```sh
$ git clone https://tecnologias.libres.cc/lablivre/mapa-colaborativo.git
```

Entre na pasta do projeto:
```sh
$ cd mapa-colaborativo
```

Inicialize o projeto (esse comando inicializa os containers e espelha o conteúdo do projeto):
```sh
$ make setup
```

## Para realizar alterações

Ao realizar alterações no projeto na parte do `wp-admin`, antes de fazer o commit, não se esqueça de atualizar o init.sql. 
Para fazer essa atualização, rode o seguinte comando pelo terminal: 
```sh
$ make backup
```
Após esse comando, faça o commit e suba as alterações.

## Outros comandos
- Atualizar o banco a partir do arquivo init.sql:
```sh
$ make restore
```

- Mostrar quais containers que estão ativos no momento:
```sh
$ make status
```

- Parar e destruir os containers que estão ativos no momento:
```sh
$ make down
```

**Made with [Dillinger](https://dillinger.io/)**